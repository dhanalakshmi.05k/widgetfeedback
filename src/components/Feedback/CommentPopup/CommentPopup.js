import React, { Component } from 'react';
import { Button, Modal } from 'react-bootstrap';
import PropTypes from 'prop-types';
import './style.css';

/**
 * ErrorModal component used to error details in a popup
 */
class CommentPopup extends Component {
    constructor (props) {
        super(props);
        this.state = {
            showModal: props.showModal
        };
        this.handleClose = this.handleClose.bind(this);
    }

    /**
     * Life cycle method for invoked whenever component receives new prop
     * @param {Object} props, updated component props
     * */
    static getDerivedStateFromProps (props, state) {
        return {
            showModal: props.showModal
        };
    }

    /**
     * Handler method triggered when user closes the popup
     */
    handleClose () {
        this.setState({
            showModal: false
        });
    }

    /**
     * Method to render widget based on the state
     */
    render () {
        const state = this.state;
        return (
            < div>
                <Modal show={state.showModal} mr-eye-widget-type="Popup"
                    dialogClassName="modal-70w"
                    centered
                    aria-labelledby="contained-modal-title-vcenter"
                    onHide= {this.props.onClickHide} >
                    <Modal.Header >
                        <span className="app-widget-control float-right">
                            <span className="display-flex">
                                <span className="display-flex">
                                    <div>
                                        <span className="fa fa-warning warning-icon"></span>
                                        <span className="warning-Header">Invalid selection</span>
                                    </div>
                                </span>
                            </span>
                        </span>
                    </Modal.Header>
                    <Modal.Body >
                       Pop up
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.props.onClickHide} mr-eye-widget-type="Button" >
                            Close
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div >
        );
    }
}

/** component default props with their data types */
CommentPopup.propTypes = {
    showModal: PropTypes.bool
};

/** component default props with their values */
CommentPopup.defaultProps = {
    showModal: false
};

export default CommentPopup;
