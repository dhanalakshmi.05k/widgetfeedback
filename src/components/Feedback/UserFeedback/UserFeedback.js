import React, { Component } from 'react';
import './style.css';
// import { getUserFeedback } from './../../selectors/userSelector';
import PropTypes from 'prop-types';
// import {
//     saveFeedbackCollection
// } from './../../actions/userFeedbackActions';
/**
 * ConnectivityDropDown component to display th select the option a switch
 */

 import CommentPopup  from '../CommentPopup'
class UserFeedback extends Component {
    constructor (props) {
        super(props);
        this.state = {
            feedbackDetails: props.feedbackDetails,
            widgetName: props.widgetName,
            dashboardName: props.dashboardName,
            thumbsUp: props.thumbsUp,
            thumbsDown: props.thumbsDown,
            showModal: props.showModal
        };
        this.clickedThumbsUp = this.clickedThumbsUp.bind(this);
        this.clickedThumbsDown = this.clickedThumbsDown.bind(this);
        this.clickedComments = this.clickedComments.bind(this);
    }
    static getDerivedStateFromProps (currProps) {
        return {
            widgetName: currProps.widgetName,
            dashboardName: currProps.dashboardName
        };
    }
    clickedComments(){
        console.log("clicked comments")
        this.setState({ showModal: true }, () => {
        })
        console.log(this.state.showModal)
    }
    clickedThumbsUp(){
        let obj={
            widgetName: this.state.widgetName,
            dashboardName: this.state.dashboardName,
            thumbsUp:true
        }
        console.log(obj)

    }
    clickedThumbsDown(){
        let obj={
            widgetName: this.state.widgetName,
            dashboardName: this.state.dashboardName,
            thumbsDown:false
        }
        console.log(obj)

    }
    addModelClose = () => this.setState({ showModal: false });
    render () {
        return (
            <span>
            <span className="feedback-fa-icon">
               <i className="fa fa-comments" onClick={this.clickedComments}  aria-hidden="true"></i>
            </span>
            <span className="feedback-fa-icon">
                <i className="fa fa-thumbs-o-up" onClick={this.clickedThumbsUp} aria-hidden="true"></i>
            </span>
           <span className="feedback-fa-icon">
            <i className="fa fa-thumbs-o-down" onClick={this.clickedThumbsDown}  aria-hidden="true"></i>
            </span>
              <CommentPopup  showModal={this.state.showModal} 
               onClickHide={this.addModelClose}></CommentPopup>
            </span>
        );
    }
}

// const mapStateToProps = state => {
//     return {
//         feedbackDetails: getUserFeedback(state)
//     };
// };
// const mapDispatchToProps = (dispatch) => {
//     return {
//         saveFeedbackCollection: (keyMarketList) => dispatch(saveFeedbackCollection(keyMarketList))
//     };
// };
UserFeedback.propTypes = {
    feedbackDetails: PropTypes.object,
    widgetName: PropTypes.string,
    dashboardName: PropTypes.string,
    thumbsUp: PropTypes.bool,
    thumbsDown: PropTypes.bool,
    showModal: PropTypes.bool
}
UserFeedback.defaultProps = {
    showModal: false
}
export default UserFeedback;
//export default (connect(mapStateToProps, mapDispatchToProps)(UserFeedback));

