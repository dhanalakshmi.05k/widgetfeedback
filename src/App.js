import React from 'react';
import logo from './logo.svg';
import './App.css';
import UserFeedback  from '../src/components/Feedback/UserFeedback';

function App() {
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <UserFeedback widgetName="IB Exceutive" dashboardName="IB"></UserFeedback>
    </div>
  );
}

export default App;
